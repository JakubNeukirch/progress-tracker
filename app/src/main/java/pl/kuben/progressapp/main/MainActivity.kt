package pl.kuben.progressapp.main

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.progressapp.R
import pl.kuben.progressapp.about.AboutActivity
import pl.kuben.progressapp.add.AddActivity.Companion.newAddIntent
import pl.kuben.progressapp.base.BaseActivity
import pl.kuben.progressapp.plain.NO_ID
import pl.kuben.progressapp.plain.PlainFragment
import pl.kuben.progressapp.splash.SplashActivity
import pl.kuben.progressapp.webParent.WebParentFragment

private const val SPLASH_CODE = 1337

class MainActivity : BaseActivity<MainViewModel>() {

    override val viewModel: MainViewModel by viewModel()

    private val plainFragment: PlainFragment by lazy { PlainFragment() }
    private val webParentFragment: WebParentFragment by lazy { WebParentFragment() }

    private val supportDialog: MainSupportDialog by lazy {
        MainSupportDialog(this).apply {
            onDontShowClick = {
                viewModel.disableSupportDialog()
                it.dismiss()
            }
            onAboutClick = {
                it.dismiss()
                openAboutScreen()
            }
        }
    }

    private val currentScreen: Screen
        get() {
            return when {
                plainFragment.isVisible -> Screen.PLAIN
                webParentFragment.isVisible -> Screen.WEB
                else -> Screen.NONE
            }
        }


    private val toggle: ActionBarDrawerToggle by lazy {
        ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupScreen()
    }

    private fun setupScreen() {
        setupToolbar()
        subscribeToSplashScreen()
        subscribeToShowDialog()
        viewModel.splashScreen()
        setFragment(viewModel.lastScreen)
        addFab.setOnClickListener { openAddScreen() }
        aboutTextView.setOnClickListener { openAboutScreen() }
    }

    private fun subscribeToSplashScreen() {
        viewModel.shouldOpenSplashScreen.observe(this, Observer {
            if (it == true) {
                openSplashScreen()
            }
        })
    }

    private fun subscribeToShowDialog() {
        viewModel.shouldShowSupportDialog.observe(this, Observer {
            if (it == true) {
                showSupportDialog()
            }
        })
    }

    private fun showSupportDialog() {
        supportDialog.show()
    }

    private fun openSplashScreen() {
        val intent = Intent(this, SplashActivity::class.java)
        startActivityForResult(intent, SPLASH_CODE)
    }

    private fun openAboutScreen() {
        startActivity(Intent(this, AboutActivity::class.java))
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_24dp)
        drawerLayout.addDrawerListener(toggle)
        navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.mainScreenItem -> {
                    setFragment(Screen.PLAIN)
                    drawerLayout.closeDrawer(GravityCompat.START)
                    true
                }
                R.id.webChartScreenItem -> {
                    setFragment(Screen.WEB)
                    drawerLayout.closeDrawer(GravityCompat.START)
                    true
                }
                else -> true
            }
        }
        toggle.syncState()
    }

    private fun setFragment(screen: Screen) {
        if (screen != currentScreen) {
            when (screen) {
                Screen.PLAIN -> openFragment(plainFragment)
                Screen.WEB -> openFragment(webParentFragment)
                else -> {
                }
            }
        }
    }

    private fun openFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
    }

    override fun onBackPressed() {
        when {
            drawerLayout.isDrawerOpen(GravityCompat.START) -> drawerLayout.closeDrawer(GravityCompat.START)
            supportFragmentManager.backStackEntryCount <= 1 -> finish()
            else -> super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return when (item.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SPLASH_CODE && resultCode == Activity.RESULT_OK) {
            viewModel.checkShowingDialog()
        }
    }

    private fun openAddScreen(progressId: Int = NO_ID) = startActivity(newAddIntent(currentScreen, progressId))

    enum class Screen(val type: Int) {
        NONE(-1),
        PLAIN(0),
        WEB(1);

        companion object {
            fun typeOf(type: Int): Screen {
                return values().find {
                    it.type == type
                } ?: PLAIN
            }
        }
    }
}
