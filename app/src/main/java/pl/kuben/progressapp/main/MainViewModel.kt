package pl.kuben.progressapp.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.common.Settings
import pl.kuben.progressapp.common.daysBefore
import pl.kuben.progressapp.common.isToday
import pl.kuben.progressapp.data.ResourceProvider

private const val SHOW_DIALOG_DAYS_INTERVAL = 2

class MainViewModel(
        resourceProvider: ResourceProvider,
        private val settings: Settings
) : BaseViewModel(resourceProvider) {

    private val _shouldOpenSplashScreen = MutableLiveData<Boolean>()
    val shouldOpenSplashScreen: LiveData<Boolean> by lazy { _shouldOpenSplashScreen }
    private val _shouldShowSupportDialog = MutableLiveData<Boolean>()
    val shouldShowSupportDialog: LiveData<Boolean> by lazy { _shouldShowSupportDialog }

    val lastScreen: MainActivity.Screen
        get() = MainActivity.Screen.typeOf(settings.lastScreen)

    private var lastOpened
        get() = settings.lastOpened
        set(value) {
            settings.lastOpened = value
        }

    fun splashScreen() {
        if (!lastOpened.isToday()) {
            _shouldOpenSplashScreen.value = true
        } else {
            checkShowingDialog()
        }

        lastOpened = System.currentTimeMillis()
    }

    fun checkShowingDialog() {
        if (settings.supportDialogLastShown.daysBefore() >= SHOW_DIALOG_DAYS_INTERVAL && settings.shouldShowSupportDialog) {
            _shouldShowSupportDialog.value = true
            settings.supportDialogLastShown = System.currentTimeMillis()
        }
    }

    fun disableSupportDialog() {
        settings.shouldShowSupportDialog = false
    }
}