package pl.kuben.progressapp.plain

import android.arch.lifecycle.MutableLiveData
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.common.Settings
import pl.kuben.progressapp.common.standardWatch
import pl.kuben.progressapp.data.Repository
import pl.kuben.progressapp.data.ResourceProvider
import pl.kuben.progressapp.data.model.db.Entry
import pl.kuben.progressapp.data.model.db.Progress
import pl.kuben.progressapp.data.model.db.ProgressCount
import pl.kuben.progressapp.main.MainActivity
import pl.kuben.progressapp.view.STYLE_PAINT
import timber.log.Timber
import java.util.*

class PlainViewModel(
        private val repository: Repository,
        private val settings: Settings,
        resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {
    val progresses = MutableLiveData<List<ProgressCount>>()

    fun onCreate() {
        if (!settings.wasOpened) {
            addExample()
            settings.wasOpened = true
        }
    }

    private fun addExample() {
        addProgress(
                Progress(
                        getString(R.string.name_hint),
                        10,
                        getColor(R.color.primary),
                        getColor(R.color.primaryDark),
                        STYLE_PAINT,
                        System.currentTimeMillis()
                )
        )
        insertEntry(Entry(6, System.currentTimeMillis(), 1))
    }

    fun onResume() {
        loadData()
        settings.lastScreen = MainActivity.Screen.PLAIN.type
    }

    private fun loadData() {
        disposables += repository.getProgressCounts()
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            progresses.value = it
                        },
                        onError = {
                            Timber.e(it)
                        }
                )
    }

    private fun addProgress(progress: Progress) {
        disposables += repository.insertProgress(progress)
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            loadData()
                        },
                        onError = {
                            Timber.e(it)
                        }
                )
    }

    fun addEntry(progress: ProgressCount, count: Int) {
        if (progress.count + count <= progress.maxValue) {
            insertEntry(Entry(count, System.currentTimeMillis(), progress.id!!))
        }
    }

    fun deleteLastEntry(progressId: Int) {
        disposables += repository.deleteEntry(progressId)
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            loadData()
                        },
                        onError = {
                            message = getString(R.string.error)
                        }
                )
    }

    fun deleteProgress(progressId: Int) {
        disposables += repository.deleteProgress(progressId)
                .andThen(repository.deleteProgressWebEntities(progressId))
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            loadData()
                        },
                        onError = {
                            Timber.e("Error ${it.message}")
                        }
                )
    }

    private fun insertEntry(entry: Entry) {
        disposables += repository.insertEntry(entry)
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            loadData()
                        }
                )
    }
}