package pl.kuben.progressapp.add.progress

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.style_spinner_item.view.*
import pl.kuben.progressapp.R
import pl.kuben.progressapp.view.*

class StyleSpinnerAdapter : BaseAdapter() {

    private val resource = R.layout.style_spinner_item

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        if (convertView == null) {
            val view = LayoutInflater.from(parent.context).inflate(resource, parent, false)
            view.spinnerProgress.style = getItem(position)
            return view
        }
        return convertView
    }

    override fun getItem(position: Int): Int = if (position < count) styles[position] else -1

    override fun getItemId(position: Int): Long = 0L

    override fun getCount(): Int = styles.size

    companion object {
        private val styles = arrayOf(STYLE_GRADIENT, STYLE_WAVE, STYLE_PAINT, STYLE_ZIGZAG, STYLE_HIVE, STYLE_BUBBLES)

        fun itemToPosition(item: Int): Int {
            styles.forEachIndexed { index, style ->
                if (style == item) {
                    return index
                }
            }
            return -1
        }

        fun positionToItem(position: Int): Int = styles[position]
    }
}