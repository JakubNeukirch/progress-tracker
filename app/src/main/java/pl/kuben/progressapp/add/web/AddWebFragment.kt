package pl.kuben.progressapp.add.web

import android.arch.lifecycle.Observer
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import kotlinx.android.synthetic.main.fragment_add_web.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.progressapp.R
import pl.kuben.progressapp.add.AddConnector
import pl.kuben.progressapp.add.progress.AddProgressFragment.Companion.ARG_PROGRESS_ID
import pl.kuben.progressapp.base.BaseFragment
import pl.kuben.progressapp.plain.NO_ID

class AddWebFragment : BaseFragment<AddWebViewModel>(), AddConnector {

    override val viewModel: AddWebViewModel by viewModel()

    private var _isEdited = false
    override val isEdited: Boolean
        get() = _isEdited

    private var webProgressId: Int = NO_ID
    private val adapter = AddProgressesAdapter()
    private var color: Int = Color.RED
        set(value) {
            field = value
            colorButton.drawable.setTint(value)
        }

    companion object {
        fun newInstance(progressId: Int = NO_ID): AddWebFragment {
            return AddWebFragment().apply {
                arguments = bundleOf(ARG_PROGRESS_ID to progressId)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_web, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        webProgressId = arguments?.getInt(ARG_PROGRESS_ID, NO_ID) ?: NO_ID
        color = ContextCompat.getColor(activity!!, R.color.defaultProgressPrimaryColor)
        setupRecyclerView()
        setupListeners()
        subscribeToProgresses()
        subscribeToClose()
        viewModel.loadData()
    }

    private fun setupRecyclerView() {
        choosingProgressRecyclerView.adapter = adapter
        choosingProgressRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    private fun setupListeners() {
        addButton.setOnClickListener {
            _isEdited = false
            viewModel.addWebProgress(webNameEditText.text.toString(), color, adapter.selectedItems)
        }
        colorButton.setOnClickListener {
            openColorPicker(color) { chosen ->
                color = chosen
                _isEdited = true
            }
        }
        webNameEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) = Unit
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                _isEdited = true
            }
        })
        adapter.onSelected = {
            _isEdited = true
        }
    }

    private fun subscribeToProgresses() {
        viewModel.progresses.observe(this, Observer { adapter.progresses = it!! })
    }

    private fun subscribeToClose() {
        viewModel.shouldClose.observe(this, Observer {
            if (it == true) {
                activity?.onBackPressed()
            }
        })
    }
}