package pl.kuben.progressapp.add

interface AddConnector {
    val isEdited: Boolean
}