package pl.kuben.progressapp.add

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_add.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.progressapp.R
import pl.kuben.progressapp.add.progress.AddProgressFragment
import pl.kuben.progressapp.add.web.AddWebFragment
import pl.kuben.progressapp.base.BaseActivity
import pl.kuben.progressapp.main.MainActivity
import pl.kuben.progressapp.plain.NO_ID

const val KEY_PROGRESS_ID = "progress_id"
const val KEY_SCREEN = "screen"

class AddActivity : BaseActivity<AddViewModel>() {

    override val viewModel: AddViewModel by viewModel()
    override val snackBarRoot: View
        get() {
            return snackBarLayout
        }

    private val confirmationDialog: Dialog by lazy {
        AlertDialog.Builder(this)
                .setMessage(R.string.dialog_close_confirmation)
                .setPositiveButton(R.string.dialog_ok) { dialog, _ ->
                    dialog.dismiss()
                    finish()
                }
                .setNegativeButton(R.string.dialog_cancel) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()

    }

    private lateinit var connector: AddConnector

    companion object {
        fun Context.newAddIntent(screen: MainActivity.Screen, progressId: Int = NO_ID): Intent {
            return Intent(this, AddActivity::class.java).apply {
                putExtra(KEY_SCREEN, screen.type)
                putExtra(KEY_PROGRESS_ID, progressId)
            }
        }

        fun Context.newAddProgressIntent(progressId: Int = NO_ID): Intent {
            return Intent(this, AddActivity::class.java).apply {
                putExtra(KEY_SCREEN, MainActivity.Screen.PLAIN.type)
                putExtra(KEY_PROGRESS_ID, progressId)
            }
        }

        fun Context.newAddWebProgressIntent(progressId: Int = NO_ID): Intent {
            return Intent(this, AddActivity::class.java).apply {
                putExtra(KEY_SCREEN, MainActivity.Screen.WEB.type)
                putExtra(KEY_PROGRESS_ID, progressId)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupParams()
        setContentView(R.layout.activity_add)
        setupFragment()
    }

    private fun setupFragment() {
        val screen = intent.getIntExtra(KEY_SCREEN, NO_ID)
        val progressId = intent.getIntExtra(KEY_PROGRESS_ID, NO_ID)
        val fragment: Fragment = when (screen) {
            MainActivity.Screen.PLAIN.type -> AddProgressFragment.newInstance(progressId)
            MainActivity.Screen.WEB.type -> AddWebFragment.newInstance(progressId)
            else -> throw IllegalArgumentException("There is no such screen for value $progressId." +
                    "\n Try other screens: Screen.Plain = 0 or Screen.Web = 1.")
        }
        openFragment(fragment)
    }

    private fun openFragment(fragment: Fragment) {
        connector = fragment as AddConnector
        supportFragmentManager.beginTransaction()
                .replace(R.id.addFragmentRoot, fragment)
                .commit()
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP && outOfBounds(event)) {
            checkAndClose()
        }
        return super.dispatchTouchEvent(event)
    }

    private fun checkAndClose() {
        if (!connector.isEdited) {
            finish()
        } else {
            confirmationDialog.show()
        }
    }

    override fun onBackPressed() {
        checkAndClose()
    }

    private fun setupParams() {
        window.setDimAmount(0.5f)
        val params: WindowManager.LayoutParams = window.attributes
        params.height = WindowManager.LayoutParams.WRAP_CONTENT
        params.gravity = Gravity.BOTTOM
        window.attributes = params
    }

    private fun outOfBounds(event: MotionEvent): Boolean {
        return event.y < addRoot.y
    }
}