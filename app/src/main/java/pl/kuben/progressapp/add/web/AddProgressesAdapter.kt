package pl.kuben.progressapp.add.web

import android.support.annotation.ColorInt
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.recycler_item_add_progress.view.*
import pl.kuben.progressapp.R
import pl.kuben.progressapp.data.model.db.ProgressCount

class AddProgressesAdapter : RecyclerView.Adapter<AddProgressesAdapter.ViewHolder>() {

    var progresses = listOf<ProgressCount>()
        set(value) {
            field = value
            selectedPositions.clear()
            notifyDataSetChanged()
        }

    var onSelected: (position: Int) -> Unit = {}

    private val selectedPositions = mutableListOf<Int>()
    val selectedItems: List<ProgressCount>
        get() = selectedPositions.map { progresses[it] }

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(container.context)
                .inflate(R.layout.recycler_item_add_progress, container, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = progresses.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(progresses[position])
    }

    private fun itemClicked(position: Int) {
        onSelected(position)
        if (selectedPositions.contains(position)) {
            selectedPositions.remove(position)
        } else {
            selectedPositions.add(position)
        }
        notifyItemChanged(position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        init {
            itemView.setOnClickListener {
                itemClicked(adapterPosition)
            }
        }

        fun bind(item: ProgressCount) {
            itemView.cardViewRoot.setCardBackgroundColor(getBackground())
            itemView.titleTextView.text = item.name
        }

        @ColorInt
        private fun getBackground(): Int {
            val colorId = if (selectedPositions.contains(adapterPosition))
                R.color.selectedItem
            else
                R.color.unSelectedItem
            return ContextCompat.getColor(itemView.context, colorId)
        }
    }
}