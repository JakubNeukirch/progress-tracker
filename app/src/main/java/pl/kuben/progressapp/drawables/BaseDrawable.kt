package pl.kuben.progressapp.drawables

import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Drawable

abstract class BaseDrawable(
        protected val resources: Resources,
        protected val style: Int
) : Drawable() {
    var progressBackgroundColor: Int = Color.WHITE
    protected val rect: Rect
        get() = bounds
    protected val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    abstract fun invalidate()

    override fun getOpacity(): Int = PixelFormat.TRANSPARENT

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
    }
}