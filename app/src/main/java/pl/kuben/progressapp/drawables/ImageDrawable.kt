package pl.kuben.progressapp.drawables

import android.content.res.Resources
import android.graphics.*

class ImageDrawable(
        resources: Resources,
        style: Int
) : GraphicDrawable(resources, style) {

    override fun draw(canvas: Canvas) {
        invalidate()
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        canvas.drawRect(rect, paint)
    }

    override fun setColorFilter(colorFilter: ColorFilter?) = Unit

    override fun setColorFilter(color: Int, mode: PorterDuff.Mode) = Unit

}