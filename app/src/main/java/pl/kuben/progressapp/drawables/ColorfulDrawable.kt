package pl.kuben.progressapp.drawables

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable

class ColorfulDrawable(resources: Resources, style: Int, private var colors: IntArray = intArrayOf(Color.RED, Color.BLACK)) : BaseDrawable(resources, style) {

    private var gradient: GradientDrawable = GradientDrawable(GradientDrawable.Orientation.BL_TR, colors)

    override fun invalidate() {
        gradient.bounds = rect
        gradient.colors = colors
    }

    override fun draw(canvas: Canvas) {
        invalidate()
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        gradient.draw(canvas)
    }
}