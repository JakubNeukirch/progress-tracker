package pl.kuben.progressapp.data

import pl.kuben.progressapp.R
import pl.kuben.progressapp.data.model.Contributor

object ContributorsRepository {

    val contributors = listOf(
            Contributor(
                    name = "JakubNeukirch",
                    descriptionId = R.string.contributor_me,
                    libraryGithubUrl = "https://github.com/JakubNeukirch",
                    twitterUrl = "https://twitter.com/Kubenqpl"
            ),
            Contributor(
                    name = "oli107",
                    descriptionId = R.string.contributor_oli,
                    libraryGithubUrl = "https://github.com/oli107/material-range-bar"
            ),
            Contributor(
                    name = "JakeWharton",
                    descriptionId = R.string.contributor_wharton,
                    libraryGithubUrl = "https://github.com/JakeWharton/timber",
                    webUrl = "https://jakewharton.com/",
                    twitterUrl = "https://twitter.com/JakeWharton"
            ),
            Contributor(
                    name = "QuadFlask",
                    descriptionId = R.string.contributor_quadflask,
                    libraryGithubUrl = "https://github.com/QuadFlask/colorpicker"
            ),
            Contributor(
                    name = "chrisjenx",
                    descriptionId = R.string.contributor_chris,
                    libraryGithubUrl = "https://github.com/chrisjenx/Calligraphy",
                    webUrl = "http://chrisjenx.com/",
                    twitterUrl = "https://twitter.com/chrisjenx"
            ),
            Contributor(
                    name = "InsertKoinIO",
                    descriptionId = R.string.contributor_koin,
                    libraryGithubUrl = "https://github.com/InsertKoinIO/koin",
                    webUrl = "https://insert-koin.io/",
                    twitterUrl = "https://twitter.com/insertkoin_io"
            ),
            Contributor(
                    name = "ReactiveX",
                    descriptionId = R.string.contributor_reactivex,
                    libraryGithubUrl = "https://github.com/ReactiveX/RxJava",
                    webUrl = "http://reactivex.io/",
                    twitterUrl = "https://twitter.com/ReactiveX"
            ),
            Contributor(
                    name = "hdodenhof",
                    descriptionId = R.string.contributor_hdodenhof,
                    twitterUrl = "https://twitter.com/hdodenhof",
                    webUrl = "https://about.me/henningdodenhof",
                    libraryGithubUrl = "https://github.com/hdodenhof/CircleImageView"
            )
    )

}