package pl.kuben.progressapp.data

import android.support.annotation.ColorRes

interface ResourceProvider {
    fun getString(stringId: Int): String
    fun getString(stringId: Int, vararg args: Any): String
    fun getColor(@ColorRes colorId: Int): Int
}