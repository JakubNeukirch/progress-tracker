package pl.kuben.progressapp.data.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.ColorInt

@Entity(tableName = "progress")
data class Progress(
        val name: String,
        val maxValue: Int,
        @ColorInt val color: Int,
        @ColumnInfo(name = "background_color") @ColorInt
        val backgroundColor: Int,
        val style: Int,
        val timestamp: Long,
        val type: Int = 0
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}