package pl.kuben.progressapp.data.model

data class Contributor(
        val name: String,
        val descriptionId: Int,
        val libraryGithubUrl: String = "",
        val webUrl: String = "",
        val twitterUrl: String = ""
)