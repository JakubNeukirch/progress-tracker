package pl.kuben.progressapp.data

import android.content.res.Resources

class AndroidResourceProvider(private val resources: Resources) : ResourceProvider {
    override fun getString(stringId: Int): String = resources.getString(stringId)
    override fun getString(stringId: Int, vararg args: Any): String = resources.getString(stringId, *args)
    override fun getColor(colorId: Int): Int = resources.getColor(colorId)
}