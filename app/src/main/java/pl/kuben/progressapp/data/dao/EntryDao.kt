package pl.kuben.progressapp.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Single
import pl.kuben.progressapp.data.model.db.Entry

@Dao
interface EntryDao {

    @Insert
    fun insert(entry: Entry): Long

    @Query("SELECT * FROM entry")
    fun getAll(): Single<List<Entry>>

    @Query("SELECT * FROM entry WHERE progressId = :progressId")
    fun getEntries(progressId: Int): Single<List<Entry>>

    @Query("SELECT SUM(count) FROM entry WHERE progressId = :progressId")
    fun countProgress(progressId: Int): Single<Int>

    @Query("DELETE FROM entry WHERE progressId = :progressId")
    fun deleteEntries(progressId: Int): Int

    @Query("DELETE FROM entry WHERE progressId = :progressId AND id = (SELECT Max(id) FROM entry)")
    fun deleteEntry(progressId: Int): Int
}