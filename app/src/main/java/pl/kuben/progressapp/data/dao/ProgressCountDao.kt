package pl.kuben.progressapp.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.reactivex.Single
import pl.kuben.progressapp.data.model.db.ProgressCount

@Dao
interface ProgressCountDao {
    @Query("SELECT p.id, p.name, p.maxValue, p.color, COALESCE(Sum(e.count), 0) as count, p.background_color, p.style, p.timestamp FROM progress p LEFT JOIN entry e on e.progressId = p.id Group By p.id")
    fun getAll(): Single<List<ProgressCount>>

    @Query("SELECT p.id, p.name, p.maxValue, p.color, COALESCE(Sum(e.count), 0) as count, p.background_color, p.style, p.timestamp " +
            "FROM progress p LEFT JOIN entry e on e.progressId = p.id " +
            "WHERE p.id = :progressId " +
            "Group By p.id")
    fun getProgressCount(progressId: Int): Single<List<ProgressCount>>

    @Query("SELECT p.id, p.name, p.maxValue, p.color, COALESCE(Sum(e.count), 0) as count, p.background_color, p.style, p.timestamp " +
            "FROM progress p LEFT JOIN entry e on e.progressId = p.id " +
            "WHERE p.id IN (:progressId) " +
            "Group By p.id")
    fun getProgressCounts(progressId: List<Long>): Single<List<ProgressCount>>
}