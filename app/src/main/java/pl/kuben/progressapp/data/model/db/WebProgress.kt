package pl.kuben.progressapp.data.model.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity()
data class WebProgress(
        val name: String,
        val color: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
}