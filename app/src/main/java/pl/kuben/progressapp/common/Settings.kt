package pl.kuben.progressapp.common

import android.content.SharedPreferences
import pl.kuben.progressapp.main.MainActivity

const val KEY_WAS_OPENED = "was_opened"
const val KEY_LAST_OPENED = "last_opened"
const val KEY_DIALOG_LAST_SHOWN = "last_shown"
const val KEY_SHOULD_SHOW_DIALOG = "should_show_dialog"
const val KEY_LAST_SCREEN = "last_screen"

class Settings constructor(private val prefs: SharedPreferences) {
    var wasOpened: Boolean
        get() = prefs.getBoolean(KEY_WAS_OPENED, false)
        set(value) {
            prefs.edit()
                    .putBoolean(KEY_WAS_OPENED, value)
                    .apply()
        }

    var lastOpened: Long
        get() = prefs.getLong(KEY_LAST_OPENED, 0)
        set(value) {
            prefs.edit()
                    .putLong(KEY_LAST_OPENED, value)
                    .apply()
        }

    var supportDialogLastShown: Long
        get() = prefs.getLong(KEY_DIALOG_LAST_SHOWN, System.currentTimeMillis())
        set(value) {
            prefs.edit()
                    .putLong(KEY_DIALOG_LAST_SHOWN, value)
                    .apply()
        }

    var shouldShowSupportDialog: Boolean
        get() = prefs.getBoolean(KEY_SHOULD_SHOW_DIALOG, true)
        set(value) {
            prefs.edit()
                    .putBoolean(KEY_SHOULD_SHOW_DIALOG, value)
                    .apply()
        }

    var lastScreen: Int
        get() = prefs.getInt(KEY_LAST_SCREEN, MainActivity.Screen.PLAIN.type)
        set(value) = prefs.edit().putInt(KEY_LAST_SCREEN, value).apply()
}