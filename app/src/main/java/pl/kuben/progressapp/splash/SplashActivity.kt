package pl.kuben.progressapp.splash

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseActivity

class SplashActivity : BaseActivity<SplashViewModel>() {

    override val viewModel: SplashViewModel by viewModel()
    private val player by lazy { MediaPlayer.create(this, R.raw.click_sound) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setupScreen()
    }

    private fun setupScreen() {
        subscribeToShouldAnimate()
        subscribeToShouldPlay()
        viewModel.startAnimationTimer()
        setResult(Activity.RESULT_OK, Intent())
    }

    private fun subscribeToShouldAnimate() {
        viewModel.shouldAnimate.observe(this, Observer {
            if (it == true) {
                turnAnimation()
            }
        })
    }

    private fun subscribeToShouldPlay() {
        viewModel.shouldPlay.observe(this, Observer {
            if (it == true) {
                playAndClose()
            }
        })
    }

    private fun turnAnimation() {
        val animation = AnimationUtils.loadAnimation(this, R.anim.splash_end)
        animation.fillAfter = true
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) = Unit
            override fun onAnimationEnd(animation: Animation?) = Unit
            override fun onAnimationStart(animation: Animation?) {
                viewModel.startPlayerTimer()
            }
        })
        logoImageView.startAnimation(animation)
    }

    private fun playAndClose() {
        player.setOnCompletionListener { finish() }
        player.start()
    }

    override fun onDestroy() {
        player.release()
        super.onDestroy()
    }
}